<!--
To replace with correct values:
- PROJECTNAME
- SONARTOKEN
- VERSION

Emojis: https://www.webfx.com/tools/emoji-cheat-sheet/
Record terminal: https://asciinema.org/
-->

<div align="center">
	<img src="logo.png" width="30%" alt="project's logo" style='fill: #94d31b'>
</div>

---

<div align="center">

__A simple, one-line project description__

</div>

<div align="center">
    <img src='https://img.shields.io/badge/Version-VERSION-red'/>
    <img src='https://img.shields.io/badge/ESP32-%E2%9C%93%20compatible-brightgreen?style=flat'>
    <img src='https://img.shields.io/badge/ESP01-%E2%9C%93%20compatible-brightgreen?style=flat'>
    <img src='https://img.shields.io/badge/ESP12E-%E2%9C%93%20compatible-brightgreen?style=flat'>
</div>

<div align="center">

<img src='https://img.shields.io/badge/Built%20with-C%2B%2B-blue?style=for-the-badge&logo=C%2B%2B'> <img src='https://img.shields.io/badge/Made%20with-LOVE-red?style=for-the-badge'>
</div>

# Table of content
[[_TOC_]]

# About PROJECTNAME
## What is PROJECTNAME
Vestibulum eu venenatis magna. Quisque dictum dapibus magna non ultrices. Ut magna mi, gravida in risus sit amet, dignissim laoreet enim. Pellentesque felis mauris, pretium id varius eu, bibendum eu nunc. Nam nec libero sit amet nibh ornare faucibus. Suspendisse potenti. Cras pretium, dui rutrum porta vulputate, odio magna rhoncus dolor, efficitur placerat dui odio ac purus. Sed congue eros vitae rutrum pulvinar.

## Installation
Vestibulum eu venenatis magna. Quisque dictum dapibus magna non ultrices. Ut magna mi, gravida in risus sit amet, dignissim laoreet enim. Pellentesque felis mauris, pretium id varius eu, bibendum eu nunc. Nam nec libero sit amet nibh ornare faucibus. Suspendisse potenti. Cras pretium, dui rutrum porta vulputate, odio magna rhoncus dolor, efficitur placerat dui odio ac purus. Sed congue eros vitae rutrum pulvinar.

## How to use it?
Vestibulum eu venenatis magna. Quisque dictum dapibus magna non ultrices. Ut magna mi, gravida in risus sit amet, dignissim laoreet enim. Pellentesque felis mauris, pretium id varius eu, bibendum eu nunc. Nam nec libero sit amet nibh ornare faucibus. Suspendisse potenti. Cras pretium, dui rutrum porta vulputate, odio magna rhoncus dolor, efficitur placerat dui odio ac purus. Sed congue eros vitae rutrum pulvinar.

## Tech considerations
### Performance
Vestibulum ultricies nunc mi, ut fringilla libero facilisis ac. Donec consequat libero lectus, eu pharetra orci commodo ac. Donec commodo nisi sit amet maximus fringilla. Nulla ornare finibus scelerisque. Aenean nec viverra quam. Maecenas nec porttitor neque. Aenean id ornare turpis.

### Dependencies
Aenean feugiat eros in vehicula condimentum. Morbi congue purus eget tristique laoreet. Mauris quis ultrices nisi. Maecenas lacinia pellentesque ultricies. Mauris ipsum orci, pulvinar eu nunc quis, finibus accumsan leo. Vestibulum quis fermentum tortor. Maecenas diam lorem, pretium ac interdum id, porttitor ac velit. In rhoncus at nisi eget commodo. Ut eget mauris in nunc posuere sollicitudin. Nullam auctor felis nec ipsum maximus, vitae accumsan risus imperdiet.

## Roadmap
Curabitur bibendum, nisi at aliquam egestas, odio mi maximus massa, ac convallis massa mauris rhoncus nulla. Maecenas lacinia sed felis et hendrerit. Curabitur semper lorem ac neque convallis, vitae blandit libero rhoncus. Morbi faucibus bibendum urna, et rhoncus justo vehicula a. Fusce vel ultricies felis, nec posuere ligula. In eget congue sem.

## Known issues
Suspendisse aliquam volutpat consequat. Vestibulum posuere maximus lacus nec vehicula. Donec sollicitudin volutpat fringilla. Suspendisse quis sollicitudin diam. Integer ligula ligula, consectetur eget turpis lobortis, varius finibus orci. Nullam vestibulum auctor faucibus.

## Licence
<div align="center">

![CC](https://mirrors.creativecommons.org/presskit/icons/cc.svg) ![BY](https://mirrors.creativecommons.org/presskit/icons/by.svg) ![SA](https://mirrors.creativecommons.org/presskit/icons/sa.svg)

</div>

This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

## Support
If you find this code useful and appreciate my work, feel free to click the button below to donate and help me continue. Every cent is appreciated! Thank you! :ok_hand:
<div align="center">

[![](donate.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_donations&business=6EX6SCMSYPF94&currency_code=EUR&source=url)

</div>


## Code quality

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=bugs'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=code_smells'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=coverage'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=duplicated_lines_density'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=ncloc'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=sqale_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=alert_status'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=reliability_rating'/>
</div>

<div align="center">
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=security_rating'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=sqale_index'/>
    <img src='https://sonar.x99.fr/api/project_badges/measure?project=PROJECTNAME&metric=vulnerabilities'/>
</div>


# Credits
Project logo made by <a href="http://www.freepik.com/" title="Freepik">Freepik</a> from <a href="https://www.flaticon.com/" title="Flaticon"> www.flaticon.com</a>

# About the author

- [Twitter](https://twitter.com/therealX99)
- [Linkedin](https://www.linkedin.com/in/pierre-vernaeckt/)
- My online resume:

<div align="center">
	<a href='https://pierre.vernaeckt.fr'>
		<img width='200' src='https://static.x99.fr/ULm9SWsQ6gzupZ.svg'>
	</a>
</div>

